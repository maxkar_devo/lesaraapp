package com.lesara.lesaraapp.activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;

import com.lesara.lesaraapp.R;
import com.lesara.lesaraapp.component.adapter.ProductsAdapter;
import com.lesara.lesaraapp.constant.LesaraAppConstants;
import com.lesara.lesaraapp.model.dto.ProductDto;
import com.lesara.lesaraapp.model.dto.TrendDto;
import com.lesara.lesaraapp.service.ProductObtainingService;
import com.lesara.lesaraapp.util.RestUtil;
import com.lesara.lesaraapp.util.ViewUtil;

import java.util.LinkedList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;


public class LesaraGoodsActivity extends AppCompatActivity {

    //Recycle view adapter
    private ProductsAdapter productsAdapter = null;

    //Recycle view layout manager
    private GridLayoutManager gridLayoutManager;

    //Retrofit service instance
    private final ProductObtainingService obtainingService = RestUtil.getApiConnectionService();

    //Page counter
    private int currentPageCount = 1;

    //Layout manager counters
    private int previousItemCounter, currentItemCounter, totalItemCounter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lesara_goods);

        ViewUtil.actionBarHandle(this);
        ViewUtil.statusBarHandle(this);

        RecyclerView rvItems = (RecyclerView) findViewById(R.id.rvProducts);

        gridLayoutManager = ViewUtil.handleScreenPosition(rvItems, gridLayoutManager, this);

        loadPreliminaryProductSet(rvItems);

        rvItems.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(final RecyclerView recyclerView, int dx, int dy) {
                if (dy > 0) {

                    initCounters();

                    if ((currentItemCounter + previousItemCounter) >= totalItemCounter) {
                        loadScrolledProductList(recyclerView);
                    }
                }
            }
        });

    }

    /**
     * Load first batch of data
     *
     * @param rvItems obtained recyclerView object
     */
    private void loadPreliminaryProductSet(final RecyclerView rvItems) {

        Call<TrendDto> call = obtainingService.getProductsAsync(String.valueOf(currentPageCount));
        call.enqueue(new Callback<TrendDto>() {
            @Override
            public void onResponse(Call<TrendDto> call, Response<TrendDto> response) {

                if (response.isSuccessful()) {
                    TrendDto trendDto = response.body();

                    productsAdapter = new ProductsAdapter(new LinkedList<>(trendDto.getTrendProductsDto().getProducts()));
                    rvItems.setAdapter(productsAdapter);
                    currentPageCount++;
                }
            }

            @Override
            public void onFailure(Call<TrendDto> call, Throwable t) {
                Log.e(LesaraAppConstants.LogTags.ERROR_TAG, t.getMessage());
            }
        });

    }

    /**
     * Load new products on each scroll event
     *
     * @param recyclerView obtained recyclerView object
     */
    private void loadScrolledProductList(final RecyclerView recyclerView) {

        obtainingService.getProducts(String.valueOf(currentPageCount))
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<TrendDto>() {
                    @Override
                    public final void onCompleted() {
                        // do nothing
                    }

                    @Override
                    public final void onError(Throwable e) {
                        Log.e(LesaraAppConstants.LogTags.APPLICATION_TAG, e.getMessage());
                    }

                    @Override
                    public final void onNext(TrendDto response) {
                        LinkedList<ProductDto> productList = productsAdapter.getProducts();
                        productList.addAll(response.getTrendProductsDto().getProducts());
                        productsAdapter.setProducts(productList);

                        recyclerView.getAdapter().notifyDataSetChanged();
                    }
                });
        currentPageCount++;
    }

    /**
     * obtain layoutManager counters values
     */
    private void initCounters() {

        currentItemCounter = gridLayoutManager.getChildCount();
        totalItemCounter = gridLayoutManager.getItemCount();
        previousItemCounter = gridLayoutManager.findFirstVisibleItemPosition();
    }
}
