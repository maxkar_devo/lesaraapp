package com.lesara.lesaraapp.application;

import android.app.Application;
import android.content.Context;

import lombok.Getter;
import lombok.Setter;

/**
 * Application class implementation
 */


public class LesaraApp extends Application {

    private static LesaraApp lesaraApp;

    private Context context;

    public static LesaraApp getInstance(){
        return lesaraApp;
    }

    public Context getContext() {
        return context;
    }

    @Override
    public void onCreate() {
        super.onCreate();

        lesaraApp = this;
        context  = getApplicationContext();
    }

}
