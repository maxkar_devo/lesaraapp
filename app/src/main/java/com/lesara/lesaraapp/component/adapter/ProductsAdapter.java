package com.lesara.lesaraapp.component.adapter;

import android.graphics.Paint;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.lesara.lesaraapp.R;
import com.lesara.lesaraapp.application.LesaraApp;
import com.lesara.lesaraapp.constant.LesaraAppConstants;
import com.lesara.lesaraapp.model.dto.ProductDto;
import com.lesara.lesaraapp.util.BizUtil;
import com.squareup.picasso.Picasso;

import java.util.LinkedList;


/**
 * Lesara product recycle view adapter implementation.
 */

public class ProductsAdapter extends RecyclerView.Adapter<ProductsAdapter.ViewHolder> {

    //Common products list
    private LinkedList<ProductDto> products;

    public LinkedList<ProductDto> getProducts() {
        return products;
    }

    public void setProducts(LinkedList<ProductDto> products) {
        this.products = products;
    }

    public ProductsAdapter(LinkedList<ProductDto> products) {

        this.products = products;
    }

    //View holder class implementation
    public static class ViewHolder extends RecyclerView.ViewHolder {

        public final TextView productTitle;
        public final TextView productPrice;
        public final TextView productOldPrice;
        public final TextView productDiscount;
        public final ImageView productThumbnail;

        public ViewHolder(View itemView) {
            super(itemView);

            productTitle = (TextView) itemView.findViewById(R.id.productTitleTop);
            productPrice = (TextView) itemView.findViewById(R.id.productPriceTop);
            productOldPrice = (TextView)itemView.findViewById(R.id.productPriceOld);
            productDiscount = (TextView)itemView.findViewById(R.id.discountPercent);
            productThumbnail = (ImageView) itemView.findViewById(R.id.productThumbnailTop);
        }

    }

    @Override
    public ProductsAdapter.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {

        return new ViewHolder(LayoutInflater.from(viewGroup.getContext()).
                inflate(R.layout.item_product_rv, viewGroup, false));
    }

    @Override
    public void onBindViewHolder(ProductsAdapter.ViewHolder viewHolder,final int position) {

        final ProductDto product = products.get(position);

        TextView productTitle = viewHolder.productTitle;
        TextView productPrice = viewHolder.productPrice;
        TextView productOldPrice = viewHolder.productOldPrice;
        TextView productDiscount = viewHolder.productDiscount;
        ImageView imageView = viewHolder.productThumbnail;

        Picasso.with(imageView.getContext())
                .load(LesaraAppConstants.LesaraEndpoints.THUMBNAIL_BASE_URL + product.getThumbnailUrl())
                .into(imageView);

        if (BizUtil.validateData(product.getTitle())) {
            productTitle.setText(product.getTitle());
        }
        if (BizUtil.validateData(product.getPrice())) {
            productPrice.setText("€" + BizUtil.formatProductsPrice(product.getPrice()));
        }
        if (BizUtil.validateData(product.getOldPrice())) {
            productOldPrice.setText("€" + BizUtil.formatProductsPrice(product.getOldPrice()));
        }
        productOldPrice.setPaintFlags(productOldPrice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
        if (BizUtil.validateData(product.getDiscount())) {
            productDiscount.setText("-" + product.getDiscount() + "%");
        }

        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(LesaraApp.getInstance().getContext(), products.get(position).getTitle(), Toast.LENGTH_SHORT).show();
            }
        });

    }

    @Override
    public int getItemCount() {
        return this.products.size();
    }

}
