package com.lesara.lesaraapp.service;

import com.lesara.lesaraapp.constant.LesaraAppConstants;
import com.lesara.lesaraapp.model.dto.TrendDto;


import retrofit2.Call;

import retrofit2.http.GET;
import retrofit2.http.Query;
import rx.Observable;

/**
 * Retrofit requests signatures implementation
 */
public interface ProductObtainingService {

    @GET(LesaraAppConstants.LesaraEndpoints.LESARA_DATA_ENDPOINT)
    Observable<TrendDto> getProducts(@Query("page_override") String page);

    @GET(LesaraAppConstants.LesaraEndpoints.LESARA_DATA_ENDPOINT)
    Call<TrendDto> getProductsAsync(@Query("page_override") String page);
}
