package com.lesara.lesaraapp.constant;

/**
 * Lesara app primary constants description
 */
public class LesaraAppConstants {

    public static class LesaraEndpoints {

        public static final String THUMBNAIL_BASE_URL = "https://daol3a7s7tps6.cloudfront.net/";

        public static final String LESARA_BASE_URL = "https://test4.lesara.de/restapi/";

        public static final String LESARA_DATA_ENDPOINT = "v1/trendproducts/?app_token=this_is_an_app_token&" +
                "user_token=63a12a8116814a9574842515378c93c64846fc3d0858def78388be37e127cd17&store_id=1";
    }

    public static class LogTags {

        public static final String APPLICATION_TAG = "LESARA_APPLICATION";

        public static final String ERROR_TAG = "ERROR";

        public static final String INFO_TAG = "INFO";

    }

    @Deprecated
    public static class PrimaryStrings {

        public static final String APPLICATION_TITLE= "Lesara";

        public static final String APPLICATION_SUBTITLE= "Everything you love. At the best price";

    }

    public static class PrimaryMetrics {

        public static final String APPLICATION_COLOR= "#00b7be";

        public static final String ACTION_BAR_COLOR= "#ffffff";

    }

}
