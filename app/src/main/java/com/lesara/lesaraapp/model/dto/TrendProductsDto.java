package com.lesara.lesaraapp.model.dto;


import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.Set;

import lombok.Getter;
import lombok.Setter;

/**
 * trend products dto description
 */


public class TrendProductsDto implements Serializable {

    @SerializedName("current_page")
    private int currentPage;

    @SerializedName("products")
    private Set<ProductDto> products;

    public int getCurrentPage() {
        return currentPage;
    }

    public void setCurrentPage(int currentPage) {
        this.currentPage = currentPage;
    }

    public Set<ProductDto> getProducts() {
        return products;
    }

    public void setProducts(Set<ProductDto> products) {
        this.products = products;
    }
}
