package com.lesara.lesaraapp.model.dto;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;


/**
 * Products dto description
 */
public class ProductDto implements Serializable{

    @SerializedName("name")
    private String title;

    @SerializedName("price")
    private String price;

    @SerializedName("msrp")
    private String oldPrice;

    @SerializedName("discount")
    private  String discount;

    @SerializedName("thumbnail_path")
    private String thumbnailUrl;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getOldPrice() {
        return oldPrice;
    }

    public void setOldPrice(String oldPrice) {
        this.oldPrice = oldPrice;
    }

    public String getDiscount() {
        return discount;
    }

    public void setDiscount(String discount) {
        this.discount = discount;
    }

    public String getThumbnailUrl() {
        return thumbnailUrl;
    }

    public void setThumbnailUrl(String thumbnailUrl) {
        this.thumbnailUrl = thumbnailUrl;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ProductDto that = (ProductDto) o;

        if (!title.equals(that.title)) return false;
        if (!price.equals(that.price)) return false;
        return oldPrice.equals(that.oldPrice);

    }

    @Override
    public int hashCode() {
        int result = title.hashCode();
        result = 31 * result + price.hashCode();
        result = 31 * result + oldPrice.hashCode();
        return result;
    }
}
