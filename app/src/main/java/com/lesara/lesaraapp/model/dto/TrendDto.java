package com.lesara.lesaraapp.model.dto;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;

/**
 * Trend dto description
 */

public class TrendDto implements Serializable{

    @SerializedName("trend_products")
    private TrendProductsDto trendProductsDto;

    public TrendProductsDto getTrendProductsDto() {
        return trendProductsDto;
    }

    public void setTrendProductsDto(TrendProductsDto trendProductsDto) {
        this.trendProductsDto = trendProductsDto;
    }
}
