package com.lesara.lesaraapp.util;

import android.app.Activity;
import android.content.Context;
import android.content.res.Configuration;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Surface;
import android.view.Window;
import android.view.WindowManager;

import com.lesara.lesaraapp.R;
import com.lesara.lesaraapp.constant.LesaraAppConstants;

/**
 * Application layouts & styles handling utility
 */
public class ViewUtil {

    /**
     * Application action bar styling
     *
     * @param context related activity context
     */
    public static void actionBarHandle(AppCompatActivity context) {

        context.getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_HOME |
                ActionBar.DISPLAY_SHOW_TITLE | ActionBar.DISPLAY_USE_LOGO);

        context.getSupportActionBar().setBackgroundDrawable(new ColorDrawable(Color.parseColor(LesaraAppConstants.PrimaryMetrics.ACTION_BAR_COLOR)));
        context.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        context.getSupportActionBar().setLogo(R.drawable.actionbar_img);
        context.getSupportActionBar().setDisplayHomeAsUpEnabled(false);
    }

    /**
     * Application status bar styling
     *
     * @param activity related activity context
     */
    public static void statusBarHandle(Activity activity) {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {

            Window window = activity.getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(activity.getResources().getColor(R.color.colorPrimary));
        }

    }

    /**
     * Check current screen orientation
     *
     * @param context related activity context
     * @return is current screen orientation portrait
     */
    public static boolean isPortraitScreenFormat(Context context) {
        final int rotation = ((WindowManager) context.getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay().getOrientation();
        switch (rotation) {
            case Surface.ROTATION_0:
                return true;
            case Surface.ROTATION_90:
                return false;
            case Surface.ROTATION_180:
                return true;
            default:
                return false;
        }
    }

    /**
     * Check current device type
     *
     * @param context related app context
     * @return is current device type - 'tablet' or 'handset'
     */
    public static boolean isTablet(Context context) {
        boolean xlarge = ((context.getResources().getConfiguration().screenLayout & Configuration.SCREENLAYOUT_SIZE_MASK) == 4);
        boolean large = ((context.getResources().getConfiguration().screenLayout & Configuration.SCREENLAYOUT_SIZE_MASK) == Configuration.SCREENLAYOUT_SIZE_LARGE);
        return (xlarge || large);
    }

    /**
     * Handle recycle view style in accordance with current screen size & orientation
     *
     * @param recyclerView related recyclerView
     * @param layoutManager obtained layout manager
     * @param context current activity context
     * @return new instance of layout manager
     */
    public static GridLayoutManager handleScreenPosition(RecyclerView recyclerView, GridLayoutManager layoutManager, Context context) {

        if (isTablet(context)) {
            if (isPortraitScreenFormat(context)) {
                layoutManager = new GridLayoutManager(context, 3);
                recyclerView.setLayoutManager(layoutManager);
            } else {
                layoutManager = new GridLayoutManager(context, 4);
                recyclerView.setLayoutManager(layoutManager);
            }
        } else {
            if (isPortraitScreenFormat(context)) {
                layoutManager = new GridLayoutManager(context, 2);
                recyclerView.setLayoutManager(layoutManager);
            } else {
                layoutManager = new GridLayoutManager(context, 3);
                recyclerView.setLayoutManager(layoutManager);
            }
        }

        return layoutManager;
    }

}
