package com.lesara.lesaraapp.util;

/**
 * Application data handling utility
 */
public class BizUtil{

    /**
     * Formatting of products price data
     *
     * @param price obtained price
     * @return formatted data
     */
    public static String formatProductsPrice(String price){
        return price.substring(0, price.length() - 2);
    }

    /**
     * Products data validation
     *
     * @param data obtained data
     * @return is valid data response
     */
    public static boolean validateData(String data){
        return data == null || data.isEmpty()? false : true;
    }
}
