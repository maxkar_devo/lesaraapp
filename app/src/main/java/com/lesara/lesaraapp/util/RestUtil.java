package com.lesara.lesaraapp.util;

import com.lesara.lesaraapp.constant.LesaraAppConstants;
import com.lesara.lesaraapp.service.ProductObtainingService;

import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Application retrofit requests handling utility
 */
public class RestUtil {

    /**
     * Creation of retrofit client
     *
     * @return retrofit client instance
     */
    public static ProductObtainingService getApiConnectionService(){

        return new Retrofit.Builder()
                .baseUrl(LesaraAppConstants.LesaraEndpoints.LESARA_BASE_URL)
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .build().create(ProductObtainingService.class);
    }
}
