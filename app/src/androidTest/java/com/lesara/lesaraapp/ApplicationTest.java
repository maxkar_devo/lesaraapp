package com.lesara.lesaraapp;

import android.app.Application;
import android.test.ApplicationTestCase;

/**
 * Common test class
 */
public class ApplicationTest extends ApplicationTestCase<Application> {
    public ApplicationTest() {
        super(Application.class);
    }
}